package com.lullichka.weatherapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Julia Alekseeva on 04.03.2018.
 */

public class WeatherResponse {

  @SerializedName("weather")
  @Expose
  private List<Weather> weather = null;

  @SerializedName("main")
  @Expose
  private Main main;
  @SerializedName("name")
  @Expose
  private String name;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public List<Weather> getWeather() {
    return weather;
  }

  public void setWeather(List<Weather> weather) {
    this.weather = weather;
  }

  public Main getMain() {
    return main;
  }

  public void setMain(Main main) {
    this.main = main;
  }


}
