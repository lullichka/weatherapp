package com.lullichka.weatherapp.http;

import com.lullichka.weatherapp.model.WeatherResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Julia Alekseeva on 04.03.2018.
 */

public interface APIinterface {
  @GET("data/2.5/weather/")
  Call<WeatherResponse> getWeather(
      @Query("id") String cityId,
      @Query("appid") String key,
      @Query("units") String units);

}
