package com.lullichka.weatherapp;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.lullichka.weatherapp.model.WeatherResponse;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements SettingsFragment.OnFragmentInteractionListener {

  private static final String FRAGMENT_TAG = "frag_tag";

  private static final String TAG = MainActivity.class.getSimpleName();
  private TextView tvNoNet;
  List<Fragment> activeFragments = new ArrayList<>();
  private AppSettings appsettings;


  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    tvNoNet = findViewById(R.id.tv_no_network);
    appsettings = AppSettings.getInstance(this);
    Fragment cityFrag = CityFragment.newInstance(appsettings.getString(AppSettings.Key.CITY));
    getSupportFragmentManager().beginTransaction().replace(R.id.content_frame,
        cityFrag, FRAGMENT_TAG).commit();
    activeFragments.add(cityFrag);

  }

  @Override
  protected void onResume() {
    super.onResume();
    if (!isOnline()) {
      tvNoNet.setVisibility(View.VISIBLE);
      for (Fragment fragment : activeFragments) {
        getSupportFragmentManager().beginTransaction().remove(fragment).commit();
      }
    } else if (refreshNeeded()) {
      refreshData();
    }
  }

  private boolean refreshNeeded() {
    String city = appsettings.getString(AppSettings.Key.CITY, "");
    String lastcity = appsettings.getString(AppSettings.Key.LAST_CITY, "");
    if (city.isEmpty()) {
      appsettings.set(AppSettings.Key.CITY, getString(R.string.default_city_code));
      return true;
    }
    long dataUpdated = appsettings.getLong(AppSettings.Key.LAST_UPDATE);
    boolean stale = (System.currentTimeMillis() - dataUpdated) > 30 * 60 * 1000;
    if (appsettings.getLong(AppSettings.Key.LAST_UPDATE, 0) == 0) {
      return true;
    } else if (stale) {
      return true;
    } else
      return !city.equals(lastcity);
  }

  public void refreshData() {
    String city = appsettings.getString(AppSettings.Key.CITY, null);
    Call<WeatherResponse> call = WeatherApp.getApi().getWeather(city, getString(R.string.api_key),
        "metric");
    call.enqueue(new Callback<WeatherResponse>() {
      @Override
      public void onResponse(Call<WeatherResponse> call, Response<WeatherResponse> response) {
        if (response.body() != null) {
          WeatherResponse weatherResponse = response.body();
          appsettings.set(AppSettings.Key.CITY_NAME, weatherResponse.getName());
          appsettings.set(AppSettings.Key.TEMP_AVERAGE, String.valueOf(weatherResponse.getMain().getTemp()));
          appsettings.set(AppSettings.Key.TEMP_MAX, String.valueOf(weatherResponse.getMain().getTempMax()));
          appsettings.set(AppSettings.Key.TEMP_MIN, String.valueOf(weatherResponse.getMain().getTempMin()));
          appsettings.set(AppSettings.Key.DESCRIPTION, (weatherResponse.getWeather().get(0).getDescription()));
          appsettings.set(AppSettings.Key.ICON_URL, weatherResponse.getWeather().get(0).getIcon());
          appsettings.set(AppSettings.Key.LAST_UPDATE, System.currentTimeMillis());
          appsettings.set(AppSettings.Key.CITY, appsettings.getString(AppSettings.Key.CITY));
          appsettings.set(AppSettings.Key.LAST_CITY, appsettings.getString(AppSettings.Key.CITY));
          refreshUI();
        }
      }

      @Override
      public void onFailure(Call<WeatherResponse> call, Throwable t) {
        Log.d(TAG, t.getMessage());
      }
    });
  }

  private void refreshUI() {
    /*final Handler handler = new Handler();
    handler.postDelayed(new Runnable() {
      @Override
      public void run() {
        Fragment cityFrag = CityFragment.newInstance(appsettings.getString(AppSettings.Key.CITY));
        getSupportFragmentManager().beginTransaction().replace(R.id.content_frame,
            cityFrag, FRAGMENT_TAG).commit();
        activeFragments.add(cityFrag);
      }
    }, 1000);*/
    Fragment cityFrag = CityFragment.newInstance(appsettings.getString(AppSettings.Key.CITY));
    getSupportFragmentManager().beginTransaction().replace(R.id.content_frame,
        cityFrag, FRAGMENT_TAG).commit();
    activeFragments.add(cityFrag);
  }

  public boolean isOnline() {
    ConnectivityManager cm =
        (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
    NetworkInfo netInfo = null;
    if (cm != null) {
      netInfo = cm.getActiveNetworkInfo();
    }
    return netInfo != null && netInfo.isConnectedOrConnecting();
  }


  @Override
  public void onFragmentInteraction() {
    if (refreshNeeded()) {
      refreshData();
    }
  }
}
