package com.lullichka.weatherapp;

import android.app.Application;

import com.lullichka.weatherapp.http.APIinterface;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Julia Alekseeva on 04.03.2018.
 */

public class WeatherApp extends Application {
  private static APIinterface service;

  public void onCreate() {
    super.onCreate();
    Retrofit retrofit = new Retrofit.Builder()
        .baseUrl("https://api.openweathermap.org/")
        .addConverterFactory(GsonConverterFactory.create())
        .build();
    service = retrofit.create(APIinterface.class);
  }

  public static APIinterface getApi() {
    return service;
  }
}
