package com.lullichka.weatherapp;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;


public class CityFragment extends Fragment {
  private static final String CITY_CODE = "city_code";
  private static final String TAG = CityFragment.class.getSimpleName();
  private static final String IMAGE_BASE = "http://openweathermap.org/img/w/";

  private TextView tvCityName, tvTempAverage, tvTempMin, tvTempMax, tvDecription;
  private ImageView ivIcon;
  private AppSettings appSettings;


  public CityFragment() {
    // Required empty public constructor
  }

  public static CityFragment newInstance(String city) {
    CityFragment fragment = new CityFragment();
    Bundle args = new Bundle();
    args.putString(CITY_CODE, city);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    View view = inflater.inflate(R.layout.fragment_city, container, false);
    setHasOptionsMenu(true);
    appSettings = AppSettings.getInstance(getActivity());
    tvCityName = view.findViewById(R.id.tv_cityname);
    tvTempAverage = view.findViewById(R.id.tv_degrees_av);
    tvTempMin = view.findViewById(R.id.tv_degrees_min);
    tvTempMax = view.findViewById(R.id.tv_degrees_max);
    tvDecription = view.findViewById(R.id.tv_description);
    ivIcon = view.findViewById(R.id.iv_icon);
    return view;
  }

  @Override
  public void onResume() {
    super.onResume();
    tvCityName.setText(appSettings.getString(AppSettings.Key.CITY_NAME));
    tvTempAverage.setText(appSettings.getString(AppSettings.Key.TEMP_AVERAGE) + getString(R.string.degrees));
    tvTempMax.setText(appSettings.getString(AppSettings.Key.TEMP_MAX) + getString(R.string.degrees));
    tvTempMin.setText(appSettings.getString(AppSettings.Key.TEMP_MIN) + getString(R.string.degrees));
    tvDecription.setText(appSettings.getString(AppSettings.Key.DESCRIPTION));
    String pathToIcon = IMAGE_BASE + appSettings.getString(AppSettings.Key.ICON_URL) + ".png";
    Picasso.with(getActivity()).load(pathToIcon)
        .placeholder(R.mipmap.ic_launcher_round).into(ivIcon);
  }

  @Override
  public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    super.onCreateOptionsMenu(menu, inflater);
    inflater.inflate(R.menu.menu_toolbar_main, menu);
    final MenuItem item = menu.findItem(R.id.action_settings);
    item.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
      @Override
      public boolean onMenuItemClick(MenuItem menuItem) {
        int menuitemId = menuItem.getItemId();
        switch (menuitemId) {
          case R.id.action_settings:
            Fragment settingsFrag = new SettingsFragment();
            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame,
                settingsFrag).addToBackStack(null).commit();
        }
        return true;
      }
    });
  }
}
