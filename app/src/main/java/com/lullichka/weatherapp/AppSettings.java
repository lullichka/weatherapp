package com.lullichka.weatherapp;

import android.content.Context;
import android.content.SharedPreferences;

import java.lang.ref.WeakReference;

/**
 * Created by Julia Alekseeva on 04.03.2018.
 */
public class AppSettings {

  private static final String SETTINGS_NAME = "default_settings";
  private static AppSettings sSharedPrefs;
  private SharedPreferences mPref;
  private SharedPreferences.Editor mEditor;
  private WeakReference<Context> mContextRef;

  /**
   * Class for keeping all the keys used for shared preferences in one place.
   */
  public static class Key {
    /* Recommended naming convention:
     * ints, floats, doubles, longs:
	   * SAMPLE_NUM or SAMPLE_COUNT or SAMPLE_INT, SAMPLE_LONG etc.
	   *
	   * boolean: IS_SAMPLE, HAS_SAMPLE, CONTAINS_SAMPLE
	   *
	   * String: SAMPLE_KEY, SAMPLE_STR or just SAMPLE
	   */

    //Loocation related
    public static final String CITY = "city";

    //Update related
    public static final String LAST_UPDATE = "last_update";
    public static final String LAST_CITY = "last_city";


    //Data stored
    public static final String CITY_NAME = "city_name";
    public static final String TEMP_AVERAGE = "temp_average";
    public static final String TEMP_MAX = "temp_max";
    public static final String TEMP_MIN = "temp_min";
    public static final String DESCRIPTION = "description";
    public static final String ICON_URL = "icon_url";

  }


  private AppSettings(Context context) {
    mPref = context.getSharedPreferences(SETTINGS_NAME, Context.MODE_PRIVATE);
    mContextRef = new WeakReference<>(context);
  }

  public static AppSettings getInstance(Context context) {
    if (sSharedPrefs == null) {
      sSharedPrefs = new AppSettings(context.getApplicationContext());
    }
    return sSharedPrefs;
  }

  public static AppSettings getInstance() {
    if (sSharedPrefs != null) {
      return sSharedPrefs;
    }

    //Option 1:
    throw new IllegalArgumentException("Should use getInstance(Context) at least once before using this method.");

    //Option 2:
    // Alternatively, you can create a new instance here
    // with something like this:
    // getInstance(MyCustomApplication.getAppContext());
  }

  public void set(String key, String val) {
    doEdit();
    mEditor.putString(key, val);
    doCommit();
  }

  public void set(String key, long val) {
    doEdit();
    mEditor.putLong(key, val);
    doCommit();
  }

  public void set(String key, boolean val) {
    doEdit();
    mEditor.putBoolean(key, val);
    doCommit();
  }

  public String getString(String key, String defaultValue) {
    return mPref.getString(key, defaultValue);
  }

  public String getString(String key) {
    return mPref.getString(key, null);
  }

  public long getLong(String key) {
    return mPref.getLong(key, 0);
  }

  public long getLong(String key, long defaultValue) {
    return mPref.getLong(key, defaultValue);
  }

  public boolean getBoolean(String key) {
    return mPref.getBoolean(key, false);
  }


  private void doEdit() {
    mEditor = mPref.edit();

  }

  private void doCommit() {
    mEditor.commit();
    mEditor = null;
  }

  public String getKeyFor(String key, int index) {
    return String.format(key, index);
  }

}