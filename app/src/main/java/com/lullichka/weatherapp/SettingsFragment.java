package com.lullichka.weatherapp;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends Fragment implements View.OnClickListener {
  private AppSettings appsettings;
  private OnFragmentInteractionListener mCallback;

  public SettingsFragment() {
    // Required empty public constructor
  }


  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    View view = inflater.inflate(R.layout.fragment_settings, container, false);
    setHasOptionsMenu(true);
    TextView tvCity1 = view.findViewById(R.id.tv_city1);
    TextView tvCity2 = view.findViewById(R.id.tv_city2);
    TextView tvCity3 = view.findViewById(R.id.tv_city3);
    tvCity1.setOnClickListener(this);
    tvCity2.setOnClickListener(this);
    tvCity3.setOnClickListener(this);
    appsettings = AppSettings.getInstance();
    return view;
  }

  @Override
  public void onClick(View v) {
    int id = v.getId();
    switch (id) {
      case R.id.tv_city1:
        appsettings.set(AppSettings.Key.CITY, "703448");
        Toast.makeText(getActivity(), "Kiev set as default", Toast.LENGTH_LONG).show();
        break;
      case R.id.tv_city2:
        appsettings.set(AppSettings.Key.CITY, "706483");
        Toast.makeText(getActivity(), "Harkiv set as default", Toast.LENGTH_LONG).show();
        break;
      case R.id.tv_city3:
        appsettings.set(AppSettings.Key.CITY, "687700");
        Toast.makeText(getActivity(), "Zaporizhzhya set as default", Toast.LENGTH_LONG).show();
        break;
    }
  }
  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    if (context instanceof SettingsFragment.OnFragmentInteractionListener) {
      mCallback = (SettingsFragment.OnFragmentInteractionListener) context;
    } else {
      throw new RuntimeException(context.toString()
          + " must implement OnFragmentInteractionListener");
    }
  }

  @Override
  public void onDetach() {
    super.onDetach();
    mCallback = null;
  }


  @Override
  public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    super.onCreateOptionsMenu(menu, inflater);
    inflater.inflate(R.menu.menu_toolbar_settings, menu);
    final MenuItem item = menu.findItem(R.id.action_close);
    item.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
      @Override
      public boolean onMenuItemClick(MenuItem menuItem) {
        int menuitemId = menuItem.getItemId();
        switch (menuitemId) {
          case R.id.action_close:
          mCallback.onFragmentInteraction();
        }
        return true;
      }
    });
  }
  public interface OnFragmentInteractionListener {
    void onFragmentInteraction();
  }
}
